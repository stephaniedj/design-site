import React from 'react';
import * as S from './styled';

const SideNavLinks = () => (
    <S.LinkBox>
        <S.Link href="https://patterns.bnnvara.nl" className="button">Design Kit <S.Arrow>&rarr;</S.Arrow> </S.Link>
        <S.Link href="https://github.com/bnnvara" className="button">Github Repos  <S.ArrowB>&rarr;</S.ArrowB> </S.Link>
    </S.LinkBox>
);

export default SideNavLinks;