import styled from 'styled-components';

export const Logo = styled.a`
    display: inline-block;
    color: black;
    text-decoration: none;
    line-height: 1.2;
    margin: 1rem 1rem 1rem 20px;
    white-space: nowrap;
    padding: 0 1rem 0 0;
`;

export const Search = styled.div`
    margin: 0 1rem 2rem;
    height: 2rem;
    width: auto;
    box-sizing: border-box;

    :focus{
        outline: none;
    }
`;

export const Input = styled.input`
    position: relative;
    vertical-align: top;
    height: 32px;
    border: none;
    background: url('./search.svg');
    background-color: #f4f7fb;
    color: #152935;
    text-overflow: ellipsis;
    width: 100%;
    order: 1;
    cursor: text;

    :focus {
        outline: none;
    }
`;

export const Label = styled.label`
    margin: 5px;
    position: relative;

    :before {
        content: "";
        position: absolute;
        left: 10px;
        top: 0;
        bottom: 0;
        width: 20px;
        background: url('./search.svg')
    }
`;


