import React, { Component } from 'react';
import SideNav from '../components/SideNav';
import PageHeader from '../components/PageHeader';
import PageNav from '../components/PageNav';
import Container from './styled';
import PageContent from '../components/PageContent';
import Footer from '../components/Footer';

class Overview extends Component {
  render() {
    return (
      <Container>
        <SideNav />
        <PageHeader >
        <PageNav />
        </PageHeader>
        <PageContent />
        <Footer />
      </Container>
    );
  }
}

export default Overview;
