import styled from 'styled-components';

export const Header = styled.header`
    grid-area: head;
    box-sizing: border-box;
    object-fit: contain;
    padding-left: 12.5%;
    padding-right: 12.5%;
    background-color: #f5f5f5;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
    border-bottom: 1px solid red;
    padding-bottom: 0;
    position: relative;
`;

export const Title = styled.h1`
    font-size: 3.375rem;
    font-weight: 300;
    margin: -1.15rem 0 0 -3px;
    line-height: 1.25;
    letter-spacing: 0;
    padding: 0;
`;

export const SubTitle = styled.h4`
    color: #5a6872;
    font-size: 0.875rem;
    font-weight: 400;
    margin: 4rem 0 1rem;
    white-space: nowrap;
`;
