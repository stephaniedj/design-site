import React from 'react';
import SideNavHeader from '../SideNavHeader'
import SideNavItems from '../SideNavItems'
import * as S from './styled';

const SideNav = () => (
 <S.SubNav className="subnav">
    <SideNavHeader />
    <SideNavItems />
 </S.SubNav>
);

export default SideNav;