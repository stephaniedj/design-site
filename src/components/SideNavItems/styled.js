import styled from 'styled-components';

export const Content = styled.div`
    width: 254px;
    overflow-y: scroll;
    overflow-x: hidden;
    display: block;
`;

export const MenuBox = styled.ul`
    display: block;
    padding: 0px;
    margin-bottom: 40px;
`;

export const Menu = styled.li`
    box-sizing: border-box;
    display: list-item;
    list-style: none;

    :focus {
        outline: none;
    }
`;

export const DropDown = styled.button`
    padding-bottom: 16px;
    box-sizing: border-box;
    cursor: pointer;
    height: 32px;
    padding-left: 16px;
    font-size: 16px;
    color: #565656;
    font-weight: 600;
    background-attachment: scroll;
    line-height: 1.5;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    text-decoration: none;
    border: none;

    :hover {
        background-color: #f5f5f5;
    }

    :focus {
        outline: none;
    }
`;

export const Chevron = styled.svg`
    box-sizing: border-box;
    height: 10px;
    width: 10px;
    cursor: pointer;    
`;

export const MenuList = styled.ul`
    color: black;
    padding: 0;
`;

export const MenuItem = styled.li`
    list-style: none;
    padding: 4px 4px 4px 0;
    border-left: 4px solid transparent;
    padding-left: 40px;
    font-weight: 100;

    :hover {
        background-color: #f5f5f5;
    }

    :focus {
        outline: none;
    }

    :active {
        border-left: 4px solid red;
        font-weight: bold;
    }
`;

export const Link = styled.a`
    text-decoration: none;
`;

