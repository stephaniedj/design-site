import React from 'react';
import * as S from './styled';
// import menuContents from '../../data/menuContents';

// const links = menuContents[1].items.tabs;

const PageNav = () => (
 <S.PageNavBox className="pagenav">
    <S.Tablist>
        <S.TabItem>
            <S.Link>Test</S.Link>
        </S.TabItem>
        <S.TabItem>
            <S.Link>Test 2</S.Link>
        </S.TabItem>
    </S.Tablist>
 </S.PageNavBox>
);

export default PageNav;