import styled from 'styled-components';

export const LinkBox = styled.div`
    width: 100%;
    padding: 0 1rem;
    display: flex;
    flex-direction: column;
`;

//Misschien wrappen in een dix met flex-directtion row om een pijltje 
//naast de tekst in de button te plaatsen.

export const Link = styled.a`
    text-decoration: none;
    margin: 4px 4px 16px 4px;
    width: 9rem;
    border: 2px solid red;
    color: black;
    padding: 10px 28px;
    font-size: 0.9rem;
    cursor: pointer;
    background-color: transparent;
    white-space: nowrap;
    display: flex;
    flex-direction: row;
    align-items: space-around;

    &:hover {
        background: red;
    }
`;

export const Arrow = styled.p`
   font-size: 0.9rem;
   margin-left: 50px;
 `;

export const ArrowB = styled.p`
font-size: 0.9rem;
margin-left: 30px;
`;