import styled from 'styled-components';

const Container = styled.div`
    display: grid;
    box-sizing: border-box;
    font-family: 'Exo BNNVARA', 'Verdana', 'sans-serif';

    grid-template-areas:
    "sidenav head"
    "sidenav main"
    "sidenav foot";

    grid-template-columns: 16rem 1fr;
    grid-template-rows: 14rem 1fr 10rem;
   
    height: 100vh;
    width: 100vw;
`;

export default Container;