import styled from 'styled-components';

export const ContentContainer = styled.main`
    grid-area: main;
    padding: 4rem 12.5%;
`;

export const Title = styled.h2`
    font-size: 1.75rem;
    font-weight: 100;
    padding: 1rem 0 2rem;
    margin-top: 4.5rem;
    line-height: 1.25;

    ::before {
    content: "";
    width: 5rem;
    height: .3rem;
    display: block;
    background-color: red;
    top: -.5rem;
    left: 0;
    margin-bottom: 1.5rem;
    }
`;

export const Subtitle = styled.h3 `
    font-size: 1.125rem;
    color: red;
    font-weight: 600;
    padding: 4rem 0 1rem;
`;

export const Intro = styled.p`
    font-size: .9rem;
    font-weight: bold;
    line-height: 1.5;
`;

export const Content = styled.p`
    font-size: .9rem;
    font-weight: 300;
    line-height: 1.5;
    letter-spacing: 0;
    padding: 0 0 1rem;
`;